
import colorsys

class Colour():
    BASE_LUMINANCE = 0.7
    BASE_SATURATION = 0.9
    RGB_MAX = 255

    def __init__(self, hue):
        self.hue = hue
    
    def to_rgb(self, hue, luminance, saturation):
        return tuple(map( lambda x : Colour.RGB_MAX * x,
            colorsys.hls_to_rgb(hue, luminance, saturation)
        ))
    
    def get(self, dimmed=False, desaturated=False):
        return self.to_rgb(
            self.hue,
            Colour.BASE_LUMINANCE * (0.6 if dimmed else 1.0),
            Colour.BASE_SATURATION * (0.25 if desaturated else 1.0),
        )

class ColourScheme():
    
    def __init__(self, num_colours):
        self.colours = [ Colour(1.0 * x / num_colours) for x in range(num_colours) ]
        self.colour_map = {}
        self.next_colour = 1
    
    def get(self, colour_id):
        if colour_id not in self.colour_map:
            self.colour_map[colour_id] = self.colours[self.next_colour]
            self.next_colour += 1
            if self.next_colour >= len(self.colours):
                self.next_colour = 0
        return self.colour_map[colour_id]

if __name__ == "__main__":
    import blessed
    term = blessed.Terminal()
    num_colours = 6
    scheme = ColourScheme(num_colours)

    for i in range(num_colours):
        col = scheme.get(i)
        print("{}: ".format(i) + term.color_rgb(*col.get(False, False))("Base colour"))
        print("{}: ".format(i) + term.color_rgb(*col.get(True, False))("Dimmed colour"))
        print("{}: ".format(i) + term.color_rgb(*col.get(True, True))("Dimmed, desaturated colour"))
        print("{}: ".format(i) + term.color_rgb(*col.get(False, True))("Desaturated colour"))
        print()