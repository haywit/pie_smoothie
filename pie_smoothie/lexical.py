
class Word():
    def __init__(self, text, voice):
        self.text = text
        self.spoken = False
        self.voice = voice

    def __str__(self):
        return self.text

class Line():
    def __init__(self):
        self.words = []
    
    def push(self, word):
        self.words.append(word)
    
    def to_unspoken_sentences(self):
        sentences = [Sentence()]
        for word in self.words:
            if word.spoken:
                continue
            if not sentences[-1].can_push(word):
                sentences.append(Sentence())
            word.spoken = True
            sentences[-1].push(word)
        return sentences

class Sentence():
    def __init__(self):
        self.text = ""
        self.voice = None
    
    def can_push(self, word):
        return self.voice is None or self.voice == word.voice

    def push(self, word):
        assert self.can_push(word)
        if self.text != "":
            self.text += " "
        self.text += word.text
        self.voice = word.voice