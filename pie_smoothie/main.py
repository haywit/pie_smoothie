import pie_smoothie.ui as ui
import signal

def main():
    app = ui.App()

    def signal_handler(sig, frame):
        app.signal()
    signal.signal(signal.SIGINT, signal_handler)

    app.launch()
