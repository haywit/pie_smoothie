import time
import sys
import copy
import enum

import blessed
import pie_smoothie.lexical as lexical
import pie_smoothie.utils as utils
import pie_smoothie.voice as voice
import pie_smoothie.colour_scheme as colour_scheme

class ReadingMode(enum.Enum):
    PAUSED = enum.auto()
    LINES = enum.auto()
    WORDS = enum.auto()

class AppMode(enum.Enum):
    COMMAND = enum.auto()
    TEXT = enum.auto()

class VerticalAlignment(enum.Enum):
    CENTER = enum.auto()
    LEFT = enum.auto()
    RIGHT = enum.auto()

class App():
    def __init__(self):
        self.term = blessed.Terminal()
        self.colour_scheme = colour_scheme.ColourScheme(6)
        self.lines = utils.FixedArray(50)
        self.lines.push_front(lexical.Line())
        self.buffer = ""
        self.command_buffer = ""
        self.commands = self.get_command_list()
        self.reading_mode = ReadingMode.WORDS
        self.app_mode = AppMode.TEXT
        self.overlay = None
        self.overlay_vertical_alignment = VerticalAlignment.CENTER
        self.error_msg = None
        self.speaker = voice.Speaker()
        self.scrollback_pos = None
        self.overlay_welcome()
    
    def launch(self):
        self.refresh()
        with self.term.cbreak():
            while (True):
                inp = self.term.inkey()
                self.error_msg = None
                if self.overlay:
                    self.overlay = None
                elif self.app_mode == AppMode.COMMAND:
                    if inp == '/':
                        self.switch_mode()
                    elif inp.code == self.term.KEY_ENTER:
                        self.exec_command()
                    elif inp.code in [self.term.KEY_DELETE, self.term.KEY_BACKSPACE]:
                        self.backspace()
                    elif not inp.is_sequence:
                        self.command_buffer += inp
                else:
                    if inp.code in [self.term.KEY_UP, self.term.KEY_DOWN]:
                        if self.scrollback_pos is None:
                            if inp.code == self.term.KEY_UP:
                                self.scrollback_pos = 1
                        elif inp.code == self.term.KEY_UP:
                            self.scrollback_pos = min(self.scrollback_pos + 1, len(self.lines) - 1)
                        else:
                            self.scrollback_pos = max(self.scrollback_pos - 1, 1)
                        if len(self.lines) > 1:
                            self.bring_forward_line(self.scrollback_pos)
                    else:
                        self.scrollback_pos = None
                        if inp == ' ':
                            self.push_word()
                        elif inp == '/' and self.buffer == "":
                            self.switch_mode()
                        elif inp.code == self.term.KEY_ENTER:
                            self.push_word()
                            self.lines.push_front(lexical.Line())
                        elif inp.code in [self.term.KEY_DELETE, self.term.KEY_BACKSPACE]:
                            self.backspace()
                        elif not inp.is_sequence:
                            self.buffer += inp
                self.speak()
                self.refresh()
    
    def switch_mode(self):
        self.command_buffer = ""
        if self.app_mode == AppMode.COMMAND:
            self.app_mode = AppMode.TEXT
        else:
            self.app_mode = AppMode.COMMAND
    
    def line_copy(self, line):
        l = copy.deepcopy(line)
        for word in l.words:
            word.spoken = False
        return l

    def overlay_welcome(self):
        self.overlay_vertical_alignment = VerticalAlignment.CENTER
        self.overlay = """
            Welcome to pie smoothie!

            Get typing straight away, or type /help for more info.
            Press any key to continue
        """
    
    def overlay_help(self):
        self.overlay_vertical_alignment = VerticalAlignment.CENTER
        
        longest_command_name_len = len(max(self.commands, key=lambda x : len(x.name)).name)
        longest_command_description_len = len(max(self.commands, key=lambda x : len(x.description)).description)

        # Pad out all commands and descriptions to the length of the longest
        format_string = "{{:{command_len}s}} : {{:{descr_len}s}}".format(
            command_len=longest_command_name_len,
            descr_len=longest_command_description_len
        )

        self.overlay = ("\nWelcome to pie smoothie!\n\n" +
            "This is a modal editor. Use / at the beginning of a word to switch between command mode and text mode.\n\n"+
            "Commands include:\n\n" +
            "\n".join([ format_string.format(c.name, c.description) for c in self.commands ]) + 
            "\n\nYou can shorten commands, too, if it's unambiguous (eg. \"h\" for \"help\")\n\n" +
            "Press any key to exit help.\n\n")

    def overlay_voices_help(self):
        self.overlay_vertical_alignment = VerticalAlignment.LEFT
        self.overlay = ("\nThe available voices are:\n\n" + 
            "\n".join(["{:15s} {:12s} {:s}".format(v.name, v.language_code, v.description) for v in self.speaker.voices.values()]) +
            "\n\nPress any key to exit this list.\n\n")
    
    def get_command_list(self):
        command_list = [
            Command("help", "show this help", lambda _ : self.overlay_help()),
            Command("quit", "exit the application", lambda _ : self.exit()),
            Command("words", "start speaking words as they are typed", lambda _ : self.set_reading_mode(ReadingMode.WORDS)),
            Command("lines", "speak only whole words at a time", lambda _ : self.set_reading_mode(ReadingMode.LINES)),
            Command("paused", "stop speaking (save up lines to read in one go)", lambda _ : self.set_reading_mode(ReadingMode.PAUSED)),
            Command("voices", "select a voice (use \"/voices ?\" to get a list of available voices)", self.exec_voice_command),
            RepeatLineCommand("repeat", "repeat a line (try \"/repeat 2\" or just \"/2\")", self.exec_repeat_command)
        ]
        return command_list
    
    def exec_voice_command(self, invocation):
        if len(invocation.command_args) != 1:
            self.report_error("Error: specify a voice (and only one voice) with the voices command, eg. \"/voices Alex\"")
        else:
            new_voice = invocation.command_args[0]
            if new_voice == "?":
                self.overlay_voices_help()
            elif new_voice in self.speaker.voices.keys():
                self.speaker.set_voice(new_voice)
            else:
                self.report_error("Error: unknown voice \"{}\" - try \"/voices ?\" for a list of voices".format(new_voice))
        
    def exec_repeat_command(self, invocation):
        # Allow invocation as "/2" or "/repeat 2"
        if invocation.command_name.isnumeric():
            line_number = invocation.command_name
        else:
            if len(invocation.command_args) != 1 or not invocation.command_args[0].isnumeric():
                self.report_error("Error: give a line number with the repeat command, eg. \"/repeat 2\" (or just \"/2\")")
                return
            else:
                line_number = invocation.command_args[0]
        try:
            self.bring_forward_line(int(line_number))
        except IndexError as e:
            self.report_error("Error: " + str(e))
    
    def set_reading_mode(self, reading_mode):
        self.reading_mode = reading_mode
    
    def exec_command(self):
        c = CommandInvocation(self.command_buffer, self.commands)
        if c.is_empty():
            pass
        elif c.is_ambiguous():
            self.report_error("Error: ambiguous command \"{}\" - matches these commands: {}".format(
                c.command_name,
                ", ".join([ command.name for command in c.matching_commands ])
            ))
        elif c.is_unknown():
            self.report_error("Error: unknown command \"{}\" - try /help for a list of commands".format(c.command_name))
        else:
            c.execute()
        self.switch_mode()
    
    def exit(self):
        sys.exit(0)
    
    def bring_forward_line(self, line_num):
        if line_num > 0 and line_num < len(self.lines):
            self.lines[0] = self.line_copy(self.lines[line_num])
        else:
            raise IndexError("invalid line number \"{}\"".format(line_num))

    def push_word(self):
        if len(self.buffer) > 0:
            self.lines[0].push(lexical.Word(self.buffer, self.speaker.current_voice))
            self.buffer = ""
    
    def backspace(self):
        if self.app_mode == AppMode.COMMAND:
            self.command_buffer = self.command_buffer[0:-1]
        elif len(self.buffer) > 0:
            self.buffer = self.buffer[0:-1]
        elif len(self.lines[0].words) > 0:
            self.buffer = self.lines[0].words[-1].text
            del self.lines[0].words[-1]
    
    def format_text(self, text, voice_name, dim, spoken):
        colour = self.colour_scheme.get(voice_name).get(dim, not spoken)
        return self.term.color_rgb(*colour)(text)

    def format_line(self, line, dim):
        return " ".join([self.format_text(word.text, word.voice.name, dim, word.spoken) for word in line.words])

    def refresh(self):
        entry_pos = self.term.height - 3
        status_bar_pos = self.term.height - 1
        centre_pos = self.term.height // 2
        update = self.term.home + self.term.clear

        # History
        update += self.term.move_xy(0, entry_pos)
        for idx, line in enumerate(self.lines[1:]):
            line_pos = entry_pos - idx - 1
            if line_pos < 0:
                break
            update += self.term.move_xy(0, entry_pos - idx - 1)
            update += "{:3}: ".format(idx + 1) + self.format_line(line, True)
        
        # Status bar
        if self.error_msg:
            update += self.term.move_xy(0, status_bar_pos) + self.term.white_on_red(self.term.rjust(" ")) # Colour whole line
            update += self.term.move_xy(0, status_bar_pos) + self.term.white_on_red(" " + self.error_msg)
        else:
            status_bar_colour = self.term.black_on_cyan2 if self.app_mode == AppMode.COMMAND else self.term.black_on_darkseagreen1
            update += self.term.move_xy(0, status_bar_pos) + status_bar_colour(self.term.rjust(self.speaker.current_voice.name + " is reading in mode: " + self.reading_mode.name.lower() + " "))
            update += self.term.move_xy(0, status_bar_pos) + status_bar_colour(" " + self.app_mode.name.title() + " mode")

        # Entry line - must be last to put the cursor in the right spot (excluding overlay)
        if self.app_mode == AppMode.TEXT:
            update += self.term.move_xy(0, entry_pos) + "   > " + self.format_line(self.lines[0], False)
            if len(self.lines[0].words) > 0:
                update += " "
            update += self.format_text(self.buffer, self.speaker.current_voice.name, False, False)
        else:
            update += self.term.move_xy(0, entry_pos) + self.term.cyan("   > " + self.command_buffer)

        # Overlay
        if self.overlay:
            overlay_text = self.overlay.splitlines()
            overlay_start_line = centre_pos - len(overlay_text) // 2
            for line_number, line in enumerate(overlay_text):
                if self.overlay_vertical_alignment == VerticalAlignment.CENTER:
                    text = self.term.center(line)
                elif self.overlay_vertical_alignment == VerticalAlignment.RIGHT:
                    text = self.term.rjust(line + "  ")
                elif self.overlay_vertical_alignment == VerticalAlignment.LEFT:
                    text = self.term.ljust("  " + line)

                update += self.term.move_xy(0, overlay_start_line + line_number) + self.term.white_on_darkgreen(text)
        print(update, flush=True, end="")
    
    def speak(self):
        if self.reading_mode == ReadingMode.PAUSED:
            return
        lines_to_read = reversed(self.lines if self.reading_mode == ReadingMode.WORDS else self.lines[1:])
        self.speaker.say(lines_to_read)
    
    def report_error(self, text):
        self.error_msg = text

    def signal(self):
        self.report_error("Use /quit to exit pie_smoothie")
        self.refresh()
    
class CommandInvocation():
    def __init__(self, line, command_list):
        self.original_line = line
        self.sanitised_line = line.lower().strip()
        self.line_parts = self.sanitised_line.split()
        self.command_name = self.line_parts[0]
        self.command_args = self.line_parts[1:]
        self.matching_commands = [ c for c in command_list if c.matches(self) ]
    
    def is_empty(self):
        return len(self.sanitised_line) == 0

    def is_ambiguous(self):
        return len(self.matching_commands) > 1

    def is_unknown(self):
        return len(self.matching_commands) == 0 and not self.is_empty()

    def execute(self):
        assert len(self.matching_commands) == 1
        self.matching_commands[0].execute(self)

class Command():
    def __init__(self, name, description, action):
        assert len(name.split()) == 1
        self.name = name.lower()
        self.description = description
        self.action = action

    def matches(self, invocation):
        return self.name.startswith(invocation.command_name)
    
    def execute(self, invocation):
        self.action(invocation)

class RepeatLineCommand(Command):
    def matches(self, invocation):
        return invocation.command_name.isnumeric() or super().matches(invocation)
