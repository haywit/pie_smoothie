
class FixedArray():
    def __init__(self, size):
        self.size = size
        self.members = []
    
    def push_front(self, item):
        self.members.insert(0, item)
        if len(self.members) > self.size:
            del self.members[-1]
    
    def __getitem__(self, key):
        return self.members.__getitem__(key)
    
    def __setitem__(self, key, value):
        self.members.__setitem__(key, value)
        if len(self.members) > self.size:
            del self.members[-1]
    
    def __len__(self):
        return self.members.__len__()

def remove_prefix(text, prefix):
    return text[text.startswith(prefix) and len(prefix):]