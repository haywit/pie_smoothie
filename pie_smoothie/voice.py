import subprocess
import threading
import queue

class Voice():
    def __init__(self, name, language_code, description):
        self.name = name
        self.language_code = language_code
        self.description = description

class BackgroundSpeakerThread():
    def __init__(self, to_speak_queue):
        self.to_speak_queue = to_speak_queue
        self.thread = threading.Thread(target=self._poll, daemon=True)
        self.thread.start()
    
    def _poll(self):
        while True:
            sentence = self.to_speak_queue.get()
            if sentence.text == "":
                continue
            subprocess.check_call([
                "say",
                "-v",
                sentence.voice.name,
                sentence.text
            ], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

class Speaker():
    def __init__(self):
        self.voices = self._init_voices()
        self.current_voice = self.voices["karen"]
        self._to_speak_queue = queue.SimpleQueue()
        self._background_speaker_thread = BackgroundSpeakerThread(self._to_speak_queue)

    def _init_voices(self):
        voices = {}
        voices_text = subprocess.check_output("say -v ?", shell=True).decode()
        for line in voices_text.splitlines():
            parts = line.split()
            voices[parts[0].lower()] = Voice(
                parts[0],
                parts[1],
                " ".join(parts[2:])
            )
        return voices
    
    def set_voice(self, name):
        self.current_voice = self.voices[name.lower()]
    
    def transform(self, text):
        if text == "I":
            return "i" # Don't say "capital I"
        return text
    
    def say_sentence(self, sentence):
        sentence.text = self.transform(sentence.text)
        self._to_speak_queue.put(sentence)

    def say(self, lines_to_read):
        for line in lines_to_read:
            for sentence in line.to_unspoken_sentences():
                self.say_sentence(sentence)
