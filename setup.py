import setuptools

with open("README.md", "r") as readme:
    long_description = readme.read()

setuptools.setup(
    name="pie_smoothie",
    version="0.0.1",
    author="Hayden Wittig",
    author_email="",
    description="A program for people who can't eat",
    long_description=long_description,
    long_description_content_type="text/markdown",
    entry_points={
        'console_scripts': ['pie_smoothie=pie_smoothie.main:main']
    },
    url="https://bitbucket.org/haywit/pie_smoothie/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: MacOS :: MacOS X",
    ],
    python_requires='>=3.6',
)
